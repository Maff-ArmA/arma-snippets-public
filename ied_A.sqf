COMMENT
"
	Creates IEDs on each side of a road in the camera area.
";


_pos = screenToWorld [0.5, 0.5];
_rad = screenToWorld [0.5, 0.5] distance2D screenToWorld [1, 0.5];

{
	_road = _x;
	_roadInfo = getRoadInfo _road;
	_width = (_roadInfo # 1) / 1.5;	//	Seems the best - Not too close or far from roadside.
	//	_dir is long-ways not across road, so add 90 degrees.
	_dir = ((_roadInfo # 6) getDir (_roadInfo # 7)) + 90;

	{
		_rndIED = (selectRandom ["IEDLandBig_F","IEDUrbanBig_F","IEDLandSmall_F","IEDUrbanSmall_F"]);
		
		_setPos = getPosATL _road;
		_testPos = (_setPos getPos [_x,_dir]);
		
		//	Check if there is space for the largest IED type.
		if (_testPos findEmptyPosition [0,0,"IEDUrbanBig_F"] isNotEqualTo []) then
		{
			_ied = create3DENEntity ["Object", _rndIED, [0,0,0], true];
			_ied set3DENAttribute ["Position", _testPos];
			//	Could never get snapping to surface to work!
			_ied set3DENAttribute ["Rotation", [0, 0, (random 360)]];
		};
	} forEach [-_width,_width];
} forEach (_pos nearRoads _rad);
